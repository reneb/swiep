Commit history of base project located at bitbucket.org/reneb/marturiaiba_swiep:

changeset:   37:ebe4261eb293
tag:         tip
user:        Nico <nicoflorian@uni.de>
date:        Thu Dec 10 15:59:01 2015 -0300
summary:     Added location to Church model

changeset:   36:aa9632a16b98
user:        Nico <nicoflorian@uni.de>
date:        Tue Dec 08 18:31:01 2015 -0300
summary:     Dependencies should be in VCS

changeset:   35:0f69ca2f343c
user:        Nico <nicoflorian@uni.de>
date:        Tue Dec 08 18:24:13 2015 -0300
summary:     Adding grappelli so the admin looks a little nicer

changeset:   34:e8ae82f70008
user:        Nico <nicoflorian@uni.de>
date:        Tue Dec 08 18:02:55 2015 -0300
summary:     IDE settings do not belong to a VCS

changeset:   33:5e2a0797f39d
user:        haiko <haiko.eitzen@gmail.com>
date:        Tue Nov 03 16:28:56 2015 -0300
summary:     Added first draft of Church Leader association model

changeset:   32:a4141935c824
user:        haiko <haiko.eitzen@gmail.com>
date:        Tue Nov 03 16:08:44 2015 -0300
summary:     Added Leaders view and URL, as well as several minor corrections

changeset:   31:d4c94d174fce
user:        haiko <haiko.eitzen@gmail.com>
date:        Tue Nov 03 16:08:04 2015 -0300
summary:     Added HTML for Leaders view

changeset:   30:299543e403b0
user:        haiko <haiko.eitzen@gmail.com>
date:        Tue Nov 03 10:51:46 2015 -0300
summary:     Improved Churches and Conferences HTML

changeset:   29:a63bf2b23011
user:        haiko <haiko.eitzen@gmail.com>
date:        Mon Nov 02 22:56:37 2015 -0300
summary:     Accidentally commited views previously without comment. Menu entries added for Churches and Conferences

changeset:   28:81253d0739c3
user:        haiko <haiko.eitzen@gmail.com>
date:        Mon Nov 02 22:54:40 2015 -0300
summary:     Modified breadcrumbs in Homepage

changeset:   27:86701f5462b7
user:        haiko <haiko.eitzen@gmail.com>
date:        Mon Nov 02 22:54:14 2015 -0300
summary:     eliminated file

changeset:   26:9559dbb199c8
user:        haiko <haiko.eitzen@gmail.com>
date:        Mon Nov 02 22:51:03 2015 -0300
summary:     Renamed static/sgp to static/swiep

changeset:   25:fcefadaf3470
user:        haiko <haiko.eitzen@gmail.com>
date:        Fri Oct 23 16:54:58 2015 -0300
summary:     Added conferences url

changeset:   24:c640ad487e49
user:        haiko <haiko.eitzen@gmail.com>
date:        Fri Oct 23 16:54:43 2015 -0300
summary:     Added conference list view

changeset:   23:95ae95e09350
user:        haiko <haiko.eitzen@gmail.com>
date:        Fri Oct 23 16:54:23 2015 -0300
summary:     Added conference list html

changeset:   22:7d1817709f84
user:        haiko <haiko.eitzen@gmail.com>
date:        Mon Oct 19 16:54:50 2015 -0300
summary:     Improved church list html

changeset:   21:45a4d5be63ff
user:        haiko <haiko.eitzen@gmail.com>
date:        Mon Oct 19 16:38:59 2015 -0300
summary:     Created basic church list view

changeset:   20:b0c94345d52b
user:        haiko <haiko.eitzen@gmail.com>
date:        Mon Oct 19 16:38:22 2015 -0300
summary:     Added new URL and unicode_literals to urls file

changeset:   19:a201bc6dd0f4
user:        haiko <haiko.eitzen@gmail.com>
date:        Mon Oct 19 16:37:45 2015 -0300
summary:     Uncommented breadcrumbs from workspace html

changeset:   18:c6c5be058edb
user:        haiko <haiko.eitzen@gmail.com>
date:        Mon Oct 19 16:37:00 2015 -0300
summary:     Added new HTML for church list view

changeset:   17:a1dd834fd34d
user:        haiko <haiko.eitzen@gmail.com>
date:        Wed Oct 14 13:52:05 2015 -0300
summary:     Eliminated previous icon, made corrections to HTML

changeset:   16:c81390b1f716
user:        haiko <haiko.eitzen@gmail.com>
date:        Wed Oct 14 13:48:23 2015 -0300
summary:     Added initial Homepage view and corresponding URL

changeset:   15:6f22880aadc8
user:        haiko <haiko.eitzen@gmail.com>
date:        Wed Oct 14 13:47:32 2015 -0300
summary:     Added HTML files, JS and CSS libraries and other resources

changeset:   14:1a5dd4f7e876
user:        haiko <haiko.eitzen@gmail.com>
date:        Wed Oct 07 16:49:44 2015 -0300
summary:     Filled Conference model, minor correction to Church model, added Conference to admin file

changeset:   13:e37d9186161f
user:        haiko <haiko.eitzen@gmail.com>
date:        Wed Oct 07 16:48:47 2015 -0300
summary:     Added settings for CountryField used in Leader nationality

changeset:   12:9195ecf30a55
user:        haiko <haiko.eitzen@gmail.com>
date:        Wed Oct 07 15:22:34 2015 -0300
summary:     Configured Leader model, minor correction to Church model, added Leader to admin, minor change to settings

changeset:   11:d9919ad0cb63
user:        haiko <haiko.eitzen@gmail.com>
date:        Wed Oct 07 14:10:40 2015 -0300
summary:     Removed db.sqlite3 from VCS

changeset:   10:3d9fb0e11360
user:        haiko <haiko.eitzen@gmail.com>
date:        Wed Oct 07 14:09:41 2015 -0300
summary:     Set up temporary admin file and site, reorganized models, added restrictions and Language model

changeset:   9:51f61dbc88f5
user:        haiko <haiko.eitzen@gmail.com>
date:        Wed Oct 07 14:05:37 2015 -0300
summary:     Removed migrations from VCS

changeset:   8:f69d7a7c18b5
user:        haiko <haiko.eitzen@gmail.com>
date:        Wed Oct 07 09:20:12 2015 -0300
summary:     Changes to models, specifically Denomination model

changeset:   7:f8e4cf02d2e0
user:        haiko <haiko.eitzen@gmail.com>
date:        Fri Oct 02 09:20:25 2015 -0400
summary:     migrations after changes in models

changeset:   6:816c5b04939a
user:        haiko <haiko.eitzen@gmail.com>
date:        Fri Oct 02 09:19:47 2015 -0400
summary:     added default value 0 to several fields, removed max_length from foundation_year

changeset:   5:4f18a6dd65f3
user:        haiko <haiko.eitzen@gmail.com>
date:        Fri Oct 02 08:54:40 2015 -0400
summary:     Set UTF-8 encoding; created Leader, Conference, Denomination and Language dummy classes; added ForeignKey references to denomination, conference and language fields in Church model; corrected mother_church ForeignKey reference; commented row code in Church model

changeset:   4:425e83709fb5
user:        haiko <haiko.eitzen@gmail.com>
date:        Fri Oct 02 08:51:04 2015 -0400
summary:     added swiep to Installed_Apps

changeset:   3:5bd60733459b
user:        haiko <haiko.eitzen@gmail.com>
date:        Thu Oct 01 17:48:19 2015 -0400
summary:     first draft of Church model

changeset:   2:dc517ee6b0e5
user:        haiko <haiko.eitzen@gmail.com>
date:        Thu Oct 01 17:12:42 2015 -0400
summary:     initial Django commit

changeset:   1:5a2ad48fc029
user:        Ren� Bartel <jeyqo927@gmail.com>
date:        Thu Oct 01 21:05:18 2015 +0000
summary:     README.md edited online with Bitbucket

changeset:   0:9860a78d2c51
user:        haiko <haiko.eitzen@gmail.com>
date:        Thu Oct 01 17:06:17 2015 -0400
summary:     initial PyCharm commit

