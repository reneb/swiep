# -*- coding: utf-8 -*-
#
# Copyright (C) 2017 Haiko Eitzen and Nico Epp
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

from __future__ import unicode_literals

from django.conf import settings
from django.db import transaction
from django.utils.safestring import mark_safe
from leaflet.forms.widgets import LeafletWidget
from django.contrib.auth.models import User
from models import Church, Conference, Leader, ChurchLeader, Denomination, Language
from django.forms import ModelForm, Select
from django.utils.translation import ugettext_lazy as _
from django import forms


class SelectOrCreate(Select):

    def __init__(self, btn_name='from_pastor_plus_btn', attrs=None, choices=()):
        self.btn_name = btn_name
        super(SelectOrCreate, self).__init__(attrs, choices)

    def render(self, name, value, attrs=None, choices=()):
        plus_btn = '''
        <button class="btn btn-default"
                name="{}"
                value="true"
                type="submit">
            <span class="glyphicon glyphicon-plus"></span>
        </button>
        '''.format(self.btn_name)

        select_html = super(SelectOrCreate, self).render(name, value, attrs, choices)

        return '''
        <div class="input-group">
            {}
            <div class="input-group-btn">
                {}
            </div>
        </div>
        '''.format(select_html, mark_safe(plus_btn))


class ChurchForm(ModelForm):
    """
    Used to create a Church
    """

    pastor = forms.ChoiceField(required=False, choices=Leader.all_as_choices, widget=SelectOrCreate)

    def __init__(self, *args, **kwargs):
        super(ChurchForm, self).__init__(*args, **kwargs)

        # In case of an update, when there's no data set through an failed POST or something,
        # and if this church is already in DB (has pk)
        # then we preselect the saved pastor of this church
        if self.fields['pastor'] and self.fields['pastor'].initial is None and \
                self.instance and self.instance.pk > 0:
            churchleader = ChurchLeader.objects.filter(church=self.instance.pk,
                                                       ministry=settings.DEFAULT_MINISTRY).first()
            if churchleader:
                self.fields['pastor'].initial = churchleader.leader.pk

    class Meta:
        model = Church
        fields = [
            'name', 'pastor',
            'denomination', 'conference', 'language',
            'phone_number', 'email', 'web_site', 'department',
            'city', 'address', 'map_location', 'foundation_year',
            'membership', 'assistance', 'owns_lot', 'owns_building',
            'mother_church', 'observation'
        ]
        # labels = { 'pastor': 'Nombre del Pastor' }
        widgets = {
            'map_location': LeafletWidget(attrs={'geom_type': 'POINT'}),
            'denomination': SelectOrCreate(btn_name='from_denomination_plus_btn'),
            'language':     SelectOrCreate(btn_name='from_language_plus_btn')
        }
        help_texts = {'map_location': ' '}  # Workaround: has to be True-ish, empty space isn't

    def save(self, commit=True):

        with transaction.atomic():
            # djangodoc/topics/db/transactions/#controlling-transactions-explicitly

            church = super(ChurchForm, self).save(commit=commit)

            default_ministry = settings.DEFAULT_MINISTRY
            churchleader = ChurchLeader.objects.filter(church=church,
                                                       ministry=default_ministry).first()

            pastor_id = self.cleaned_data['pastor']
            if int(pastor_id) > 0:
                pastor = Leader.objects.get(pk=pastor_id)
                if not churchleader:
                    churchleader = ChurchLeader(church=church, ministry=default_ministry)
                churchleader.leader = pastor
                churchleader.save()
            else:
                # Case if no pastor is selected in dropdown
                if churchleader:
                    churchleader.delete()

        return church


class ChurchMapForm(ModelForm):
    """
    Workaround to allow map location to be shown in read-only view
    """

    class Meta:
        model = Church
        fields = ['map_location']
        widgets = {'map_location': LeafletWidget(attrs={'geom_type': 'POINT'})}
        help_texts = {'map_location': ' '}  # Workaround: has to be True-ish, empty space isn't


class LeaderForm(ModelForm):
    """
    Used to create a Leader
    """

    hidden_data = forms.CharField(widget=forms.HiddenInput(), required=False)

    class Meta:
        model = Leader
        fields = [
            'first_names', 'last_names', 'member_of', 'birthdate',
            'nationality', 'hometown', 'current_residence', 'phone_number',
            'email', 'maximum_education_level', 'theological_studies',
            'profession', 'observation',
            'hidden_data'
        ]


class ConferenceForm(ModelForm):
    """
    Used to create a Conference
    """

    class Meta:
        model = Conference
        fields = [
            'name', 'acronym', 'denomination', 'phone_number', 'email',
            'web_site', 'department', 'city', 'address', 'foundation_year',
            'observation'
        ]


class UserForm(ModelForm):
    """
    Used to edit a User (not password)
    """

    class Meta:
        model = User
        fields = ['username', 'email', 'groups']
        labels = {
            'username': _('Nombre'),
            'email': _('Correo'),
            'groups': _('Grupo')
        }


class ChurchToLeaderForm(ModelForm):
    """
    Used to add a church and ministry to a leader
    """

    class Meta:
        model = ChurchLeader
        fields = ['church', 'ministry']


class LeaderToChurchForm(ModelForm):
    """
    Used to add a leader and ministry to a church
    """

    class Meta:
        model = ChurchLeader
        fields = ['leader', 'ministry']


class DenominationForm(ModelForm):
    """
    Used to create a Denomination
    """

    hidden_data = forms.CharField(widget=forms.HiddenInput(), required=False)

    class Meta:
        model = Denomination
        fields = ['name', 'hidden_data']


class LanguageForm(ModelForm):
    """
    Used to create a Language
    """

    hidden_data = forms.CharField(widget=forms.HiddenInput(), required=False)

    class Meta:
        model = Language
        fields = ['name', 'hidden_data']
