# -*- coding: utf-8 -*-
#
# Copyright (C) 2017 Haiko Eitzen and Nico Epp
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

from __future__ import unicode_literals

from django.conf import settings
from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
import json
from django.shortcuts import render, redirect, get_object_or_404
from django.core.urlresolvers import reverse
from django.core.exceptions import ValidationError
from django.contrib.auth.models import User, Group, Permission
from django.contrib.auth.forms import UserCreationForm, PasswordChangeForm, AdminPasswordChangeForm
from django.contrib.auth.decorators import permission_required, user_passes_test, login_required
from django.forms import modelform_factory
from models import Church, Conference, Leader, ChurchLeader, Denomination, Language
from forms import ChurchForm, ConferenceForm, LeaderForm, UserForm
from forms import ChurchMapForm, ChurchToLeaderForm, LeaderToChurchForm
from forms import DenominationForm, LanguageForm


def login_view(request):
    """
    Displays the login page used for authentication
    of a User
    :param request:
    :return:
    """
    context = dict()
    context['page_title'] = "SWIEP"

    next = ""
    if request.GET:
        next = request.GET['next']

    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                if next == "":
                    return redirect('home')
                else:
                    return redirect(next)
            else:
                messages.error(request, 'El usuario se encuentra desactivado', 'danger')
                return render(request, 'login.html', context)
        else:
            messages.error(request, 'El nombre de usuario o la contraseña son incorrectos', 'danger')
            return render(request, 'login.html', context)
    else:
        return render(request, 'login.html', context)


def logout_view(request):
    """
    Used to close the session of a User
    :param request:
    :return:
    """
    logout(request)
    return redirect('home')


def map_view(request):
    """
    Displays all churches on a world map
    :param request:
    :return:
    """
    context = {
        'churches': Church.objects.exclude(map_location__exact='')
    }
    return render(request, 'map.html', context)


def home(request):
    """
    Displays the initial page or homepage of the website
    Includes links to Churches, Conferences and Leaders
    as well as to a management page
    :param request:
    :return:
    """
    context = dict()
    context['page_title'] = "Sistema Web de Información Eclesial del Paraguay"
    context['page_name'] = "Inicio"

    # entries will be the list of links available at this page
    entries = []
    entries.append(('Iglesias', reverse('church_list')))
    if request.user.has_perm('swiep.read_leader'):
        entries.append(('Líderes', reverse('leader_list')))
    entries.append(('Convenciones', reverse('conference_list')))
    if request.user.is_authenticated():
        entries.append(('Gestión', reverse('management')))
    context['menu_entries'] = entries

    return render(request, 'home.html', context)


# code copied from https://djangosnippets.org/snippets/1618/
# see Jan. 26, 2012 comment by ryankask1
def any_permission_required(*perms):
    """
    Decorator for views that returns True if the user
    has any of the permissions specified in perms.
    :param perms:
    :return:
    """
    return user_passes_test(lambda u: any(u.has_perm(perm) for perm in perms))


@login_required()
def management(request):
    """
    Displays a management page of additional objects
    other than Churches, Leaders, and Conferences
    :param request:
    :return:
    """
    context = dict()
    context['page_title'] = "Gestión de SWIEP"
    context['breadcrumb'] = [('Inicio', reverse('home')), ]
    context['page_name'] = "Gestión"

    # entries will be the list of links available at this page
    entries = []
    entries.append(('Cambiar mi contraseña', reverse('change_password', args=[request.user.pk])))
    if request.user.has_perm('auth.add_user'):
        entries.append(('Usuarios', reverse('user_list')))
    if request.user.has_perm('auth.add_group'):
        entries.append(('Grupos', reverse('group_list')))
    if request.user.has_perm('swiep.add_denomination') or \
        request.user.has_perm('swiep.add_language'):
        entries.append(('Otros', reverse('other_list')))
    context['menu_entries'] = entries

    return render(request, 'home.html', context)


def church_list(request):
    """
    Displays the list of Churches.
    :param request: HttpRequest
    :return: HttpResponse
    """
    context = dict()
    context['page_title'] = 'Iglesias'
    context['breadcrumb'] = [('Inicio', reverse('home')),]
    context['churches'] = Church.objects.order_by('name')
    return render(request, 'church_list.html', context)


def has_pastor_btn(request):
    return has_plus_btn(request, 'pastor')


def has_denomination_btn(request):
    return has_plus_btn(request, 'denomination')


def has_language_btn(request):
    return has_plus_btn(request, 'language')


def has_plus_btn(request, object_type):
    plus_btn = request.POST.get('from_' + object_type + '_plus_btn', None)
    save_btn = request.POST.get('from_' + object_type + '_save_btn', None)
    cancel_btn = request.POST.get('from_' + object_type + '_cancel_btn', None)
    return plus_btn or save_btn or cancel_btn


def handle_plus_btn(request, church_context, object_type, church_instance=None):
    """
    Handle all the different cases originating from direct pastor, denomination
    or language creation in the church create or update form.
    
    :param church_instance: When this instance is provided the logic 
    assumes the function call comes from a church update                    
    """
    if object_type == 'pastor':
        form_name = 'Líder'
        breadcrumb = [('Inicio', reverse('home')),
                      ('Líderes', reverse('leader_list'))
                      ]
        form_class = LeaderForm
        html_render = 'leader_form.html'
        object_class = Leader
    elif object_type == 'denomination':
        form_name = 'Denominación'
        breadcrumb = [('Inicio', reverse('home')),
                      ('Gestión', reverse('management')),
                      ('Otros', reverse('other_list')),
                      ]
        form_class = DenominationForm
        html_render = 'other_form.html'
        object_class = Denomination
    elif object_type == 'language':
        form_name = 'Idioma'
        breadcrumb = [('Inicio', reverse('home')),
                      ('Gestión', reverse('management')),
                      ('Otros', reverse('other_list')),
                      ]
        form_class = LanguageForm
        html_render = 'other_form.html'
        object_class = Language

    context = dict()
    context['page_title'] = 'Agregar ' + form_name
    context['breadcrumb'] = breadcrumb
    context['object_type'] = object_type
    context['model'] = form_name.lower()

    if request.POST.get('from_' + object_type + '_plus_btn', None):

        church_data = json.dumps(request.POST)
        form = form_class(initial={'hidden_data': church_data})

        context['form'] = form
        context['CRUD'] = 'C'
        context['has_hidden_data'] = True

        return render(request, html_render, context)

    elif request.POST.get('from_' + object_type + '_save_btn', None):
        element = object_class()
        element_form = form_class(request.POST, instance=element)
        if element_form.is_valid():
            element_form.save()

            church_dict = json.loads(request.POST.get('hidden_data', '{}'))
            church_dict[object_type] = element.pk
            if church_instance:
                form = ChurchForm(church_dict, instance=church_instance)
            else:
                # We use 'initial' when we can so the form doesn't show errors after the user
                # returns from the new element create view and if he hadn't filled all church fields
                # correctly before he went to create the new element.
                form = ChurchForm(initial=church_dict)
        else:
            context['form'] = element_form
            context['CRUD'] = 'C'
            context['has_hidden_data'] = True
            return render(request, html_render, context)

    elif request.POST.get('from_' + object_type + '_cancel_btn', None):
        church_dict = json.loads(request.POST.get('hidden_data', '{}'))
        if church_instance:
            form = ChurchForm(church_dict, instance=church_instance)
        else:
            # Same case here with 'initial' as explained above..
            form = ChurchForm(initial=church_dict)

    if church_instance:
        church_context['id'] = church_instance.pk
        church_context['form'] = form
        church_context['CRUD'] = 'U'
    else:
        church_context['form'] = form
        church_context['CRUD'] = 'C'

    return render(request, 'church_form.html', church_context)


@permission_required('swiep.add_church')
def church_create(request):
    """
    Displays the data form of a church for creation
    :param request:
    :return:
    """
    context = dict()
    context['page_title'] = 'Agregar Iglesia'
    context['breadcrumb'] = [('Inicio', reverse('home')),
                             ('Iglesias', reverse('church_list'))
                            ]

    church = Church()

    if request.method == 'POST':

        if has_pastor_btn(request):
            return handle_plus_btn(request, context, 'pastor')
        elif has_denomination_btn(request):
            return handle_plus_btn(request, context, 'denomination')
        elif has_language_btn(request):
            return handle_plus_btn(request, context, 'language')
        else:
            form = ChurchForm(request.POST, instance=church)
            if form.is_valid():
                form.save()
                return redirect('church_read', church.pk)
            else:
                messages.warning(request, 'Introduzca datos válidos')
    else:
        form = ChurchForm(instance=church)

    context['form'] = form
    context['CRUD'] = 'C'

    return render(request, 'church_form.html', context)


def church_read(request, church_id):
    """
    Displays the information of a church in read-only mode
    :param request:
    :param church_id:
    :return:
    """
    context = dict()
    context['page_title'] = 'Información de Iglesia'
    context['breadcrumb'] = [('Inicio', reverse('home')),
                             ('Iglesias', reverse('church_list'))
                            ]

    church = get_object_or_404(Church, pk=church_id)
    pastor = church.leaders.filter(ministry=settings.DEFAULT_MINISTRY).first()
    churchmap = ChurchMapForm(instance=church)

    context['id'] = church_id
    context['church'] = church
    context['map'] = churchmap
    context['pastor'] = pastor
    context['CRUD'] = 'R'

    return render(request, 'church_form.html', context)


@permission_required('swiep.change_church')
def church_update(request, church_id):
    """
    Displays the data form of a church for edition
    :param request:
    :param church_id:
    :return:
    """
    context = dict()
    context['page_title'] = 'Modificar Iglesia'

    church = get_object_or_404(Church, pk=church_id)

    context['breadcrumb'] = [('Inicio', reverse('home')),
                             ('Iglesias', reverse('church_list')),
                             (church.name, reverse('church_read', args=[church_id]))
                            ]

    if request.method == 'POST':
        if has_pastor_btn(request):
            return handle_plus_btn(request, context, 'pastor', church_instance=church)
        elif has_denomination_btn(request):
            return handle_plus_btn(request, context, 'denomination', church_instance=church)
        elif has_language_btn(request):
            return handle_plus_btn(request, context, 'language', church_instance=church)
        else:
            form = ChurchForm(request.POST, instance=church)
            if form.is_valid():
                form.save()
                return redirect('church_read', church_id)
            else:
                messages.warning(request, 'Introduzca datos válidos')
    else:
        form = ChurchForm(instance=church)

    context['id'] = church_id
    context['form'] = form
    context['CRUD'] = 'U'

    return render(request, 'church_form.html', context)


@permission_required('swiep.delete_church')
def church_delete(request, church_id):
    """
    Displays a page to delete church
    :param request:
    :param church_id:
    :return:
    """
    context = dict()
    context['page_title'] = 'Eliminar Iglesia'

    church = get_object_or_404(Church, pk=church_id)

    context['breadcrumb'] = [('Inicio', reverse('home')),
                             ('Iglesias', reverse('church_list')),
                             (church.name, reverse('church_read', args=[church_id]))
                            ]

    context['church'] = church
    context['id'] = church_id

    if request.method == 'POST':
        church.delete()
        return redirect('church_list')
    else:
        context['CRUD'] = 'D'
        return render(request, 'church_form.html', context)


@permission_required('swiep.read_leader')
def leader_list(request):
    """
    Displays the list of Leaders
    :param request:
    :return:
    """
    context = dict()
    context['page_title'] = 'Líderes'
    context['breadcrumb'] = [('Inicio', reverse('home')),]
    context['leaders'] = Leader.objects.order_by('last_names')
    return render(request, 'leader_list.html', context)


@permission_required('swiep.add_leader')
def leader_create(request):
    """
    Displays the data form of a leader for creation
    :param request:
    :return:
    """
    context = dict()
    context['page_title'] = 'Agregar Líder'
    context['breadcrumb'] = [('Inicio', reverse('home')),
                             ('Líderes', reverse('leader_list'))
                            ]

    leader = Leader()

    if request.method == 'POST':
        form = LeaderForm(request.POST, instance=leader)
        if form.is_valid():
            form.save()
            return redirect('leader_read', leader.pk)
        else:
            messages.warning(request, 'Introduzca datos válidos')
    else:
        form = LeaderForm(instance=leader)

    context['form'] = form
    context['CRUD'] = 'C'

    return render(request, 'leader_form.html', context)


@permission_required('swiep.read_leader')
def leader_read(request, leader_id):
    """
    Displays the information of a leader in read-only mode
    :param request:
    :param leader_id:
    :return:
    """
    context = dict()
    context['page_title'] = 'Información de Líder'
    context['breadcrumb'] = [('Inicio', reverse('home')),
                             ('Líderes', reverse('leader_list'))
                            ]

    leader = get_object_or_404(Leader, pk=leader_id)

    context['id'] = leader_id
    context['leader'] = leader
    context['CRUD'] = 'R'

    return render(request, 'leader_form.html', context)


@permission_required('swiep.change_leader')
def leader_update(request, leader_id):
    """
    Displays the data form of a leader for edition
    :param request:
    :param leader_id:
    :return:
    """
    context = dict()
    context['page_title'] = 'Modificar Líder'

    leader = get_object_or_404(Leader, pk=leader_id)

    context['breadcrumb'] = [('Inicio', reverse('home')),
                             ('Líderes', reverse('leader_list')),
                             (leader.last_names, reverse('leader_read', args=[leader_id]))
                            ]

    if request.method == 'POST':
        form = LeaderForm(request.POST, instance=leader)
        if form.is_valid():
            form.save()
            return redirect('leader_read', leader_id)
        else:
            messages.warning(request, 'Introduzca datos válidos')
    else:
        form = LeaderForm(instance=leader)

    context['id'] = leader_id
    context['form'] = form
    context['CRUD'] = 'U'

    return render(request, 'leader_form.html', context)


@permission_required('swiep.delete_leader')
def leader_delete(request, leader_id):
    """
    Displays a page to delete leader
    :param request:
    :param leader_id:
    :return:
    """
    context = dict()
    context['page_title'] = 'Eliminar Líder'

    leader = get_object_or_404(Leader, pk=leader_id)

    context['breadcrumb'] = [('Inicio', reverse('home')),
                             ('Líderes', reverse('leader_list')),
                             (leader.last_names, reverse('leader_read', args=[leader_id]))
                            ]

    context['leader'] = leader
    context['id'] = leader_id

    if request.method == 'POST':
        leader.delete()
        return redirect('leader_list')
    else:
        context['CRUD'] = 'D'
        return render(request, 'leader_form.html', context)


def conference_list(request):
    """
    Displays the list of Conferences
    :param request:
    :return:
    """
    context = dict()
    context['page_title'] = 'Convenciones'
    context['breadcrumb'] = [('Inicio', reverse('home')),]
    context['conferences'] = Conference.objects.order_by('name')
    return render(request, 'conference_list.html', context)


@permission_required('swiep.add_conference')
def conference_create(request):
    """
    Displays the data form of a conference for creation
    :param request:
    :return:
    """
    context = dict()
    context['page_title'] = 'Agregar Convención'
    context['breadcrumb'] = [('Inicio', reverse('home')),
                             ('Convenciones', reverse('conference_list'))
                            ]

    conference = Conference()

    if request.method == 'POST':
        form = ConferenceForm(request.POST, instance=conference)
        if form.is_valid():
            form.save()
            return redirect('conference_read', conference.pk)
        else:
            messages.warning(request, 'Introduzca datos válidos')
    else:
        form = ConferenceForm(instance=conference)

    context['form'] = form
    context['CRUD'] = 'C'

    return render(request, 'conference_form.html', context)


def conference_read(request, conference_id):
    """
    Displays the information of a conference in read-only mode
    :param request:
    :param conference_id:
    :return:
    """
    context = dict()
    context['page_title'] = 'Información de Convención'
    context['breadcrumb'] = [('Inicio', reverse('home')),
                             ('Convenciones', reverse('conference_list'))
                            ]

    conference = get_object_or_404(Conference, pk=conference_id)

    context['id'] = conference_id
    context['conference'] = conference
    context['CRUD'] = 'R'

    return render(request, 'conference_form.html', context)


@permission_required('swiep.change_conference')
def conference_update(request, conference_id):
    """
    Displays the data form of a conference for edition
    :param request:
    :param conference_id:
    :return:
    """
    context = dict()
    context['page_title'] = 'Modificar Convención'

    conference = get_object_or_404(Conference, pk=conference_id)

    context['breadcrumb'] = [('Inicio', reverse('home')),
                             ('Convenciones', reverse('conference_list')),
                             (conference.acronym, reverse('conference_read', args=[conference_id]))
                            ]

    if request.method == 'POST':
        form = ConferenceForm(request.POST, instance=conference)
        if form.is_valid():
            form.save()
            return redirect('conference_read', conference_id)
        else:
            messages.warning(request, 'Introduzca datos válidos')
    else:
        form = ConferenceForm(instance=conference)

    context['id'] = conference_id
    context['form'] = form
    context['CRUD'] = 'U'

    return render(request, 'conference_form.html', context)


@permission_required('swiep.delete_conference')
def conference_delete(request, conference_id):
    """
    Displays a page to delete conference
    :param request:
    :param conference_id:
    :return:
    """
    context = dict()
    context['page_title'] = 'Eliminar Convención'

    conference = get_object_or_404(Conference, pk=conference_id)

    context['breadcrumb'] = [('Inicio', reverse('home')),
                             ('Convenciones', reverse('conference_list')),
                             (conference.name, reverse('conference_read', args=[conference_id]))
                            ]

    context['conference'] = conference
    context['id'] = conference_id

    if request.method == 'POST':
        conference.delete()
        return redirect('conference_list')
    else:
        context['CRUD'] = 'D'
        return render(request, 'conference_form.html', context)


@any_permission_required('auth.add_user',
                         'auth.change_user',
                         'auth.delete_user')
def user_list(request):
    """
    Displays the list of Users
    :param request:
    :return:
    """
    context = dict()
    context['page_title'] = 'Usuarios'
    context['breadcrumb'] = [('Inicio', reverse('home')),
                             ('Gestión', reverse('management')),
                            ]
    context['users'] = User.objects.order_by('username')
    return render(request, 'user_list.html', context)


@permission_required('auth.add_user')
def user_create(request):
    """
    Displays the data form of a user for creation.
    :param request:
    :return:
    """
    context = dict()
    context['page_title'] = 'Agregar Usuario'
    context['breadcrumb'] = [('Inicio', reverse('home')),
                             ('Gestión', reverse('management')),
                             ('Usuarios', reverse('user_list'))
                             ]

    user = User()

    if request.method == 'POST':
        form = UserCreationForm(request.POST, instance=user)
        if form.is_valid():
            user = form.save()
            return redirect('user_read', user.pk)
        else:
            messages.warning(request, 'Introduzca datos válidos')
    else:
        form = UserCreationForm(instance=user)

    context['form'] = form
    context['CRUD'] = 'C'

    return render(request, 'user_form.html', context)


@any_permission_required('auth.add_user',
                         'auth.change_user',
                         'auth.delete_user')
def user_read(request, user_id):
    """
    Displays the information of a User in read-only mode
    :param request:
    :param user_id:
    :return:
    """
    context = dict()
    context['page_title'] = 'Información de Usuario'
    context['breadcrumb'] = [('Inicio', reverse('home')),
                             ('Gestión', reverse('management')),
                             ('Usuarios', reverse('user_list'))
                            ]

    user = get_object_or_404(User, pk=user_id)

    context['id'] = user_id
    context['usr'] = user
    context['CRUD'] = 'R'

    return render(request, 'user_form.html', context)


@permission_required('auth.change_user')
def user_update(request, user_id):
    """
    Displays the data form of a User for edition
    :param request:
    :param user_id:
    :return:
    """
    context = dict()
    context['page_title'] = 'Modificar Usuario'

    user = get_object_or_404(User, pk=user_id)

    context['breadcrumb'] = [('Inicio', reverse('home')),
                             ('Gestión', reverse('management')),
                             ('Usuarios', reverse('user_list')),
                             (user.username, reverse('user_read', args=[user_id]))
                            ]

    if request.method == 'POST':
        form = UserForm(request.POST, instance=user)
        if form.is_valid():
            form.save()
            return redirect('user_read', user_id)
        else:
            messages.warning(request, 'Introduzca datos válidos')
    else:
        form = UserForm(instance=user)
        # TO DO: somehow provide initial values for Groups CheckBoxSelectMultiple
        # perhaps using initial, as follows, but the pks must be mapped to choice values
        # in the rendered template
        # form = UserForm(instance=user, initial={'groups':['1','2']})

    context['id'] = user_id
    context['form'] = form
    context['CRUD'] = 'U'

    return render(request, 'user_form.html', context)


@permission_required('auth.delete_user')
def user_delete(request, user_id):
    """
    Displays a page to delete User
    :param request:
    :param user_id:
    :return:
    """
    context = dict()
    context['page_title'] = 'Eliminar Usuario'

    user = get_object_or_404(User, pk=user_id)

    context['breadcrumb'] = [('Inicio', reverse('home')),
                             ('Gestión', reverse('management')),
                             ('Usuarios', reverse('user_list')),
                             (user.username, reverse('user_read', args=[user_id]))
                            ]

    context['usr'] = user
    context['id'] = user_id

    if request.method == 'POST':
        user.delete()
        return redirect('user_list')
    else:
        context['CRUD'] = 'D'
        return render(request, 'user_form.html', context)


@login_required()
def change_password(request, user_id):
    """
    Displays a page to change a User's password
    :param request:
    :param user_id:
    :return:
    """
    context = dict()
    context['page_title'] = 'Cambiar contraseña'

    user = get_object_or_404(User, pk=user_id)

    breadcrumb = [('Inicio', reverse('home')),
                  ('Gestión', reverse('management')),]
    if request.user.has_perm('auth.change_user'):
        breadcrumb.append(('Usuarios', reverse('user_list')))
        breadcrumb.append((user.username, reverse('user_read', args=[user_id])))

    context['breadcrumb'] = breadcrumb

    if request.user == user:
        if request.method == 'POST':
            form = PasswordChangeForm(user, request.POST)
            if form.is_valid():
                form.save()
                return redirect('login')
            else:
                messages.warning(request, 'Introduzca datos válidos')
        else:
            form = PasswordChangeForm(user)
    elif request.user.has_perm('auth.change_user'):
        if request.method == 'POST':
            form = AdminPasswordChangeForm(user, request.POST)
            if form.is_valid():
                form.save()
                return redirect('user_read', user_id)
            else:
                messages.warning(request, 'Introduzca datos válidos')
        else:
            form = AdminPasswordChangeForm(user)
    else:
        return redirect('login')

    context['usr'] = user
    context['id'] = user_id
    context['form'] = form

    return render(request, 'password_form.html', context)


@permission_required('swiep.add_churchleader')
def churchleader_create_at_leader(request, leader_id):
    """
    Displays a page from a Leader context to
    associate a Leader with a Church ministry
    :param request:
    :param leader_id:
    :return:
    """
    context = dict()
    context['page_title'] = 'Agregar Ministerio a Líder'

    leader = get_object_or_404(Leader, pk=leader_id)

    context['breadcrumb'] = [('Inicio', reverse('home')),
                             ('Líderes', reverse('leader_list')),
                             (leader.last_names, reverse('leader_read', args=[leader_id]))
                            ]

    context['leader'] = leader

    churchleader = ChurchLeader()

    if request.method == 'POST':
        form = ChurchToLeaderForm(request.POST, instance=churchleader)
        if form.is_valid():
            churchleader = form.save(commit=False)
            churchleader.leader = leader
            churchleader.save()
            return redirect('leader_read', leader_id)
        else:
            messages.warning(request, 'Introduzca datos válidos')
    else:
        form = ChurchToLeaderForm(instance=churchleader)

    context['form'] = form
    context['CRUD'] = 'C'

    return render(request, 'churchleader_at_leader_form.html', context)


@permission_required('swiep.read_leader')
def churchleader_read_at_leader(request, leader_id, churchleader_id):
    """
    Displays a simple page with the information about
    a ChurchLeader association from the context of a
    leader
    :param request:
    :param leader_id:
    :param churchleader_id:
    :return:
    """
    context = dict()
    context['page_title'] = 'Información de Ministerio'

    churchleader = get_object_or_404(ChurchLeader, pk=churchleader_id)
    leader = churchleader.leader

    context['breadcrumb'] = [('Inicio', reverse('home')),
                             ('Líderes', reverse('leader_list')),
                             (leader.last_names, reverse('leader_read', args=[leader_id]))
                            ]

    context['churchleader'] = churchleader
    context['id'] = churchleader_id
    context['leader_id'] = leader_id
    context['CRUD'] = 'R'

    return render(request, 'churchleader_at_leader_form.html', context)


@permission_required('swiep.change_churchleader')
def churchleader_update_at_leader(request, leader_id, churchleader_id):
    """
    Displays a page from a Leader context to edit an
    association of a Leader with a Church ministry
    :param request:
    :param leader_id:
    :param churchleader_id:
    :return:
    """
    context = dict()
    context['page_title'] = 'Modificar Ministerio de Líder'

    churchleader = get_object_or_404(ChurchLeader, pk=churchleader_id)
    leader = churchleader.leader

    context['breadcrumb'] = [('Inicio', reverse('home')),
                             ('Líderes', reverse('leader_list')),
                             (leader.last_names, reverse('leader_read', args=[leader_id]))
                            ]

    if request.method == 'POST':
        form = ChurchToLeaderForm(request.POST, instance=churchleader)
        if form.is_valid():
            form.save()
            return redirect('leader_read', leader_id)
        else:
            messages.warning(request, 'Introduzca datos válidos')
    else:
        form = ChurchToLeaderForm(instance=churchleader)

    context['churchleader'] = churchleader
    context['id'] = churchleader_id
    context['leader_id'] = leader_id
    context['form'] = form
    context['CRUD'] = 'U'

    return render(request, 'churchleader_at_leader_form.html', context)


@permission_required('swiep.delete_churchleader')
def churchleader_delete_at_leader(request, leader_id, churchleader_id):
    """
    Displays a page from a Leader context to delete
    an asociation of a Leader with a Church ministry
    :param request:
    :param leader_id:
    :param churchleader_id:
    :return:
    """
    context = dict()
    context['page_title'] = 'Eliminar Ministerio'

    churchleader = get_object_or_404(ChurchLeader, pk=churchleader_id)
    leader = churchleader.leader

    context['breadcrumb'] = [('Inicio', reverse('home')),
                             ('Líderes', reverse('leader_list')),
                             (leader.last_names, reverse('leader_read', args=[leader_id]))
                            ]

    context['churchleader'] = churchleader
    context['id'] = churchleader_id
    context['leader_id'] = leader_id

    if request.method == 'POST':
        churchleader.delete()
        return redirect('leader_read', leader_id)
    else:
        context['CRUD'] = 'D'
        return render(request, 'churchleader_at_leader_form.html', context)


@permission_required('swiep.add_churchleader')
def churchleader_create_at_church(request, church_id):
    """
    Displays a page from a Church context to
    associate a Leader with a Church ministry
    :param request:
    :param church_id:
    :return:
    """
    context = dict()
    context['page_title'] = 'Agregar Líder a Iglesia'

    church = get_object_or_404(Church, pk=church_id)

    context['breadcrumb'] = [('Inicio', reverse('home')),
                             ('Iglesias', reverse('church_list')),
                             (church.name, reverse('church_read', args=[church_id]))
                            ]

    context['church'] = church

    churchleader = ChurchLeader()
    churchleader.church = church

    if request.method == 'POST':
        form = LeaderToChurchForm(request.POST, instance=churchleader)
        if form.is_valid():
            form.save()
            return redirect('church_read', church_id)
        else:
            messages.warning(request, 'Introduzca datos válidos')
    else:
        form = LeaderToChurchForm(instance=churchleader)

    context['form'] = form
    context['CRUD'] = 'C'

    return render(request, 'churchleader_at_church_form.html', context)


@permission_required('swiep.read_leader')
def churchleader_read_at_church(request, church_id, churchleader_id):
    """
    Displays a simple page with the information about
    a ChurchLeader association from the context of a
    church
    :param request:
    :param church_id:
    :param churchleader_id:
    :return:
    """
    context = dict()
    context['page_title'] = 'Información de Líder en Iglesia'

    churchleader = get_object_or_404(ChurchLeader, pk=churchleader_id)
    church = churchleader.church

    context['breadcrumb'] = [('Inicio', reverse('home')),
                             ('Iglesias', reverse('church_list')),
                             (church.name, reverse('church_read', args=[church_id]))
                            ]

    context['churchleader'] = churchleader
    context['id'] = churchleader_id
    context['church_id'] = church_id
    context['CRUD'] = 'R'

    return render(request, 'churchleader_at_church_form.html', context)


@permission_required('swiep.change_churchleader')
def churchleader_update_at_church(request, church_id, churchleader_id):
    """
    Displays a page from a Church context to edit an
    association of a Leader with a Church ministry
    :param churchleader_id:
    :param request:
    :param church_id:
    :return:
    """
    context = dict()
    context['page_title'] = 'Modificar Líder en Iglesia'

    churchleader = get_object_or_404(ChurchLeader, pk=churchleader_id)
    church = churchleader.church

    context['breadcrumb'] = [('Inicio', reverse('home')),
                             ('Iglesias', reverse('church_list')),
                             (church.name, reverse('church_read', args=[church_id]))
                            ]

    if request.method == 'POST':
        form = LeaderToChurchForm(request.POST, instance=churchleader)
        if form.is_valid():
            form.save()
            return redirect('church_read', church_id)
        else:
            messages.warning(request, 'Introduzca datos válidos')
    else:
        form = LeaderToChurchForm(instance=churchleader)

    context['churchleader'] = churchleader
    context['id'] = churchleader_id
    context['church_id'] = church_id
    context['form'] = form
    context['CRUD'] = 'U'

    return render(request, 'churchleader_at_church_form.html', context)


@permission_required('swiep.delete_churchleader')
def churchleader_delete_at_church(request, church_id, churchleader_id):
    """
    Displays a page from a Church context to delete
    an asociation of a Leader with a Church ministry
    :param request:
    :param church_id:
    :param churchleader_id:
    :return:
    """
    context = dict()
    context['page_title'] = 'Eliminar Líder de Iglesia'

    churchleader = get_object_or_404(ChurchLeader, pk=churchleader_id)
    church = churchleader.church

    context['breadcrumb'] = [('Inicio', reverse('home')),
                             ('Iglesias', reverse('church_list')),
                             (church.name, reverse('church_read', args=[church_id]))
                            ]

    context['churchleader'] = churchleader
    context['id'] = churchleader_id
    context['church_id'] = church_id

    if request.method == 'POST':
        churchleader.delete()
        return redirect('church_read', church_id)
    else:
        context['CRUD'] = 'D'
        return render(request, 'churchleader_at_church_form.html', context)


@any_permission_required('swiep.add_denomination',
                         'swiep.add_language')
def other_list(request):
    """
    Displays a lists of other objects from the
    management context
    :param request:
    :return:
    """
    context = dict()
    context['page_title'] = 'Otros'
    context['breadcrumb'] = [('Inicio', reverse('home')),
                             ('Gestión', reverse('management')),
                            ]

    context['denominations'] = Denomination.objects.order_by('name')
    context['languages'] = Language.objects.order_by('name')

    return render(request, 'other_list.html', context)


@permission_required('swiep.add_denomination')
def denomination_create(request):
    """
    Displays a basic page to add a denomination
    :param request:
    :return:
    """
    context = dict()
    context['page_title'] = 'Agregar denominación'
    context['breadcrumb'] = [('Inicio', reverse('home')),
                             ('Gestión', reverse('management')),
                             ('Otros', reverse('other_list')),
                            ]

    DenominationForm = modelform_factory(Denomination, fields=('name',))
    denomination = Denomination()

    if request.method == 'POST':
        form = DenominationForm(request.POST, instance=denomination)
        if form.is_valid():
            form.save()
            return redirect('other_list')
    else:
        form = DenominationForm(instance=denomination)

    context['form'] = form
    context['model'] = 'denominación'
    context['CRUD'] = 'C'

    return render(request, 'other_form.html', context)


@permission_required('swiep.delete_denomination')
def denomination_delete(request, denomination_id):
    """
    Displays a basic page to remove a denomination
    :param request:
    :param denomination_id:
    :return:
    """
    context = dict()
    context['page_title'] = 'Eliminar denominación'
    context['breadcrumb'] = [('Inicio', reverse('home')),
                             ('Gestión', reverse('management')),
                             ('Otros', reverse('other_list')),
                             ]

    denomination = get_object_or_404(Denomination, pk=denomination_id)

    context['elem'] = denomination

    if request.method == 'POST':
        denomination.delete()
        return redirect('other_list')
    else:
        context['CRUD'] = 'D'
        return render(request, 'other_form.html', context)


@permission_required('swiep.add_language')
def language_create(request):
    """
    Displays a basic page to add a language
    :param request:
    :return:
    """
    context = dict()
    context['page_title'] = 'Agregar idioma'
    context['breadcrumb'] = [('Inicio', reverse('home')),
                             ('Gestión', reverse('management')),
                             ('Otros', reverse('other_list')),
                             ]

    LanguageForm = modelform_factory(Language, fields=('name',))
    language = Language()

    if request.method == 'POST':
        form = LanguageForm(request.POST, instance=language)
        if form.is_valid():
            form.save()
            return redirect('other_list')
    else:
        form = LanguageForm(instance=language)

    context['form'] = form
    context['model'] = 'idioma'
    context['CRUD'] = 'C'

    return render(request, 'other_form.html', context)


@permission_required('swiep.delete_language')
def language_delete(request, language_id):
    """
    Displays a basic page to remove a language
    :param request:
    :param language_id:
    :return:
    """
    context = dict()
    context['page_title'] = 'Eliminar idioma'
    context['breadcrumb'] = [('Inicio', reverse('home')),
                             ('Gestión', reverse('management')),
                             ('Otros', reverse('other_list')),
                             ]

    language = get_object_or_404(Language, pk=language_id)

    context['elem'] = language

    if request.method == 'POST':
        language.delete()
        return redirect('other_list')
    else:
        context['CRUD'] = 'D'
        return render(request, 'other_form.html', context)


@any_permission_required('auth.add_group',
                         'auth.change_group',
                         'auth.delete_group')
def group_list(request):
    """
    Displays a list of permission groups
    :param request:
    :return:
    """
    context = dict()
    context['page_title'] = 'Grupos'
    context['breadcrumb'] = [('Inicio', reverse('home')),
                             ('Gestión', reverse('management')),
                            ]

    context['groups'] = Group.objects.order_by('name')

    return render(request, 'group_list.html', context)


@permission_required('auth.add_group')
def group_create(request):
    context = dict()
    context['page_title'] = 'Agregar grupo'
    context['breadcrumb'] = [('Inicio', reverse('home')),
                             ('Gestión', reverse('management')),
                             ('Grupos', reverse('group_list')),
                             ]

    # code mostly based on SGP - project_is2
    # except now we use Django Permission model instead of own Permission model
    context['CRUD'] = 'C'
    # TO DO: filter to show just swiep and auth permissions
    # perhaps use filter with Q objects
    # http://stackoverflow.com/questions/6567831/how-to-perform-or-condition-in-django-queryset
    context['perms_available'] = Permission.objects.order_by('codename')
    context['perms_assigned'] = list()

    if request.method == 'POST':
        group = Group()
        group.name = request.POST.get('name')
        try:
            group.full_clean()
        except ValidationError as e:
            context['group'] = group
            # Below is code from original project. TO DO adapt to this project
            # utils.show_errors(request, e.message_dict)
            return render(request, 'group_form.html', context)
        group.save()
        group.permissions = request.POST.getlist('perms')
        group.save()
        return redirect('group_list')

    return render(request, 'group_form.html', context)


@any_permission_required('auth.add_group',
                         'auth.change_group',
                         'auth.delete_group')
def group_read(request, group_id):
    """
    Displays the information of a Group in read-only mode
    :param request:
    :param group_id:
    :return:
    """
    context = dict()
    context['page_title'] = 'Información de grupo'
    context['breadcrumb'] = [('Inicio', reverse('home')),
                             ('Gestión', reverse('management')),
                             ('Grupos', reverse('group_list')),
                             ]

    group = get_object_or_404(Group, pk=group_id)

    context['id'] = group_id
    context['group'] = group
    context['CRUD'] = 'R'

    return render(request, 'group_form.html', context)


@permission_required('auth.change_group')
def group_update(request, group_id):
    context = dict()
    context['page_title'] = 'Modificar grupo'
    context['breadcrumb'] = [('Inicio', reverse('home')),
                             ('Gestión', reverse('management')),
                             ('Grupos', reverse('group_list')),
                             ]

    group = get_object_or_404(Group, pk=group_id)

    # code mostly based on SGP - project_is2
    # except now we use Django Permission model instead of own Permission model
    context['CRUD'] = 'U'
    context['id'] = group_id
    context['group'] = group
    # TO DO: filter to show just swiep and auth permissions
    # perhaps use filter with Q objects
    # http://stackoverflow.com/questions/6567831/how-to-perform-or-condition-in-django-queryset
    context['perms_available'] = Permission.objects.order_by('codename')
    context['perms_assigned'] = group.permissions.all().order_by('codename')

    if request.method == 'POST':
        group.name = request.POST.get('name')
        try:
            group.full_clean()
        except ValidationError as e:
            context['group'] = group
            # Below is code from original project. TO DO adapt to this project
            # utils.show_errors(request, e.message_dict)
            return render(request, 'group_form.html', context)
        group.save()
        group.permissions = request.POST.getlist('perms')
        group.save()
        return redirect('group_read', group_id)

    return render(request, 'group_form.html', context)


@permission_required('auth.delete_group')
def group_delete(request, group_id):
    context = dict()
    context['page_title'] = 'Eliminar grupo'

    group = get_object_or_404(Group, pk=group_id)

    context['breadcrumb'] = [('Inicio', reverse('home')),
                             ('Gestión', reverse('management')),
                             ('Grupos', reverse('group_list')),
                             (group.name, reverse('group_read', args=[group_id])),
                             ]

    context['group'] = group
    context['id'] = group_id
    context['count'] = group.user_set.all().count()

    if request.method == 'POST':
        if group.user_set.all().count() < 1:
            group.delete()
        return redirect('group_list')
    else:
        context['CRUD'] = 'D'
        return render(request, 'group_form.html', context)