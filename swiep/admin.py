# -*- coding: utf-8 -*-
#
# Copyright (C) 2017 Haiko Eitzen and Nico Epp
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

from __future__ import unicode_literals
from django.contrib import admin
from leaflet.admin import LeafletGeoAdmin
from .models import Denomination
from .models import Language
from .models import Church
from .models import Leader
from .models import Conference
from .models import ChurchLeader

# Register your models here.
admin.site.register(Denomination)
admin.site.register(Language)
admin.site.register(Church, LeafletGeoAdmin)
admin.site.register(Leader)
admin.site.register(Conference)
admin.site.register(ChurchLeader)