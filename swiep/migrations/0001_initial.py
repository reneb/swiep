# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import djgeojson.fields
import django_countries.fields


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Church',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(default='', max_length=100, verbose_name='nombre', error_messages={'blank': 'El nombre no puede quedar vac\xedo'})),
                ('phone_number', models.CharField(default='', max_length=50, verbose_name='tel\xe9fono', blank=True)),
                ('email', models.EmailField(default='', max_length=254, blank=True, error_messages={'invalid': 'El formato del email no es v\xe1lido'})),
                ('web_site', models.CharField(default='', max_length=100, verbose_name='sitio web', blank=True)),
                ('department', models.CharField(default='', max_length=100, verbose_name='departamento', blank=True)),
                ('city', models.CharField(default='', max_length=100, verbose_name='ciudad', blank=True)),
                ('address', models.CharField(default='', max_length=100, verbose_name='direcci\xf3n', blank=True)),
                ('map_location', djgeojson.fields.PointField(default='', verbose_name='ubicaci\xf3n en el mapa', blank=True)),
                ('foundation_year', models.IntegerField(default=2016, verbose_name='a\xf1o de fundaci\xf3n', choices=[(1870, 1870), (1871, 1871), (1872, 1872), (1873, 1873), (1874, 1874), (1875, 1875), (1876, 1876), (1877, 1877), (1878, 1878), (1879, 1879), (1880, 1880), (1881, 1881), (1882, 1882), (1883, 1883), (1884, 1884), (1885, 1885), (1886, 1886), (1887, 1887), (1888, 1888), (1889, 1889), (1890, 1890), (1891, 1891), (1892, 1892), (1893, 1893), (1894, 1894), (1895, 1895), (1896, 1896), (1897, 1897), (1898, 1898), (1899, 1899), (1900, 1900), (1901, 1901), (1902, 1902), (1903, 1903), (1904, 1904), (1905, 1905), (1906, 1906), (1907, 1907), (1908, 1908), (1909, 1909), (1910, 1910), (1911, 1911), (1912, 1912), (1913, 1913), (1914, 1914), (1915, 1915), (1916, 1916), (1917, 1917), (1918, 1918), (1919, 1919), (1920, 1920), (1921, 1921), (1922, 1922), (1923, 1923), (1924, 1924), (1925, 1925), (1926, 1926), (1927, 1927), (1928, 1928), (1929, 1929), (1930, 1930), (1931, 1931), (1932, 1932), (1933, 1933), (1934, 1934), (1935, 1935), (1936, 1936), (1937, 1937), (1938, 1938), (1939, 1939), (1940, 1940), (1941, 1941), (1942, 1942), (1943, 1943), (1944, 1944), (1945, 1945), (1946, 1946), (1947, 1947), (1948, 1948), (1949, 1949), (1950, 1950), (1951, 1951), (1952, 1952), (1953, 1953), (1954, 1954), (1955, 1955), (1956, 1956), (1957, 1957), (1958, 1958), (1959, 1959), (1960, 1960), (1961, 1961), (1962, 1962), (1963, 1963), (1964, 1964), (1965, 1965), (1966, 1966), (1967, 1967), (1968, 1968), (1969, 1969), (1970, 1970), (1971, 1971), (1972, 1972), (1973, 1973), (1974, 1974), (1975, 1975), (1976, 1976), (1977, 1977), (1978, 1978), (1979, 1979), (1980, 1980), (1981, 1981), (1982, 1982), (1983, 1983), (1984, 1984), (1985, 1985), (1986, 1986), (1987, 1987), (1988, 1988), (1989, 1989), (1990, 1990), (1991, 1991), (1992, 1992), (1993, 1993), (1994, 1994), (1995, 1995), (1996, 1996), (1997, 1997), (1998, 1998), (1999, 1999), (2000, 2000), (2001, 2001), (2002, 2002), (2003, 2003), (2004, 2004), (2005, 2005), (2006, 2006), (2007, 2007), (2008, 2008), (2009, 2009), (2010, 2010), (2011, 2011), (2012, 2012), (2013, 2013), (2014, 2014), (2015, 2015), (2016, 2016)])),
                ('membership', models.IntegerField(default=0, verbose_name='membres\xeda')),
                ('assistance', models.IntegerField(default=0, verbose_name='asistencia')),
                ('owns_lot', models.BooleanField(default=False, verbose_name='posee terreno')),
                ('owns_building', models.BooleanField(default=False, verbose_name='posee edificio')),
                ('observation', models.CharField(default='', max_length=1000, verbose_name='observaci\xf3n', blank=True)),
            ],
            options={
                'verbose_name': 'iglesia',
                'verbose_name_plural': 'iglesias',
            },
        ),
        migrations.CreateModel(
            name='ChurchLeader',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('ministry', models.CharField(default='Pastor', max_length=1000, verbose_name='ministerio')),
                ('church', models.ForeignKey(verbose_name='iglesia', to='swiep.Church')),
            ],
        ),
        migrations.CreateModel(
            name='Conference',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(default='', max_length=100, verbose_name='nombre', error_messages={'blank': 'El nombre no puede quedar vac\xedo'})),
                ('acronym', models.CharField(default='', max_length=50, verbose_name='acr\xf3nimo', error_messages={'blank': 'El nombre no puede quedar vac\xedo'})),
                ('phone_number', models.CharField(default='', max_length=50, verbose_name='tel\xe9fono', blank=True)),
                ('email', models.EmailField(default='', max_length=254, blank=True, error_messages={'invalid': 'El formato del email no es v\xe1lido'})),
                ('web_site', models.CharField(default='', max_length=100, verbose_name='sitio web', blank=True)),
                ('department', models.CharField(default='', max_length=100, verbose_name='departamento', blank=True)),
                ('city', models.CharField(default='', max_length=100, verbose_name='ciudad', blank=True)),
                ('address', models.CharField(default='', max_length=100, verbose_name='direcci\xf3n', blank=True)),
                ('foundation_year', models.IntegerField(default=2016, verbose_name='a\xf1o de fundaci\xf3n', choices=[(1870, 1870), (1871, 1871), (1872, 1872), (1873, 1873), (1874, 1874), (1875, 1875), (1876, 1876), (1877, 1877), (1878, 1878), (1879, 1879), (1880, 1880), (1881, 1881), (1882, 1882), (1883, 1883), (1884, 1884), (1885, 1885), (1886, 1886), (1887, 1887), (1888, 1888), (1889, 1889), (1890, 1890), (1891, 1891), (1892, 1892), (1893, 1893), (1894, 1894), (1895, 1895), (1896, 1896), (1897, 1897), (1898, 1898), (1899, 1899), (1900, 1900), (1901, 1901), (1902, 1902), (1903, 1903), (1904, 1904), (1905, 1905), (1906, 1906), (1907, 1907), (1908, 1908), (1909, 1909), (1910, 1910), (1911, 1911), (1912, 1912), (1913, 1913), (1914, 1914), (1915, 1915), (1916, 1916), (1917, 1917), (1918, 1918), (1919, 1919), (1920, 1920), (1921, 1921), (1922, 1922), (1923, 1923), (1924, 1924), (1925, 1925), (1926, 1926), (1927, 1927), (1928, 1928), (1929, 1929), (1930, 1930), (1931, 1931), (1932, 1932), (1933, 1933), (1934, 1934), (1935, 1935), (1936, 1936), (1937, 1937), (1938, 1938), (1939, 1939), (1940, 1940), (1941, 1941), (1942, 1942), (1943, 1943), (1944, 1944), (1945, 1945), (1946, 1946), (1947, 1947), (1948, 1948), (1949, 1949), (1950, 1950), (1951, 1951), (1952, 1952), (1953, 1953), (1954, 1954), (1955, 1955), (1956, 1956), (1957, 1957), (1958, 1958), (1959, 1959), (1960, 1960), (1961, 1961), (1962, 1962), (1963, 1963), (1964, 1964), (1965, 1965), (1966, 1966), (1967, 1967), (1968, 1968), (1969, 1969), (1970, 1970), (1971, 1971), (1972, 1972), (1973, 1973), (1974, 1974), (1975, 1975), (1976, 1976), (1977, 1977), (1978, 1978), (1979, 1979), (1980, 1980), (1981, 1981), (1982, 1982), (1983, 1983), (1984, 1984), (1985, 1985), (1986, 1986), (1987, 1987), (1988, 1988), (1989, 1989), (1990, 1990), (1991, 1991), (1992, 1992), (1993, 1993), (1994, 1994), (1995, 1995), (1996, 1996), (1997, 1997), (1998, 1998), (1999, 1999), (2000, 2000), (2001, 2001), (2002, 2002), (2003, 2003), (2004, 2004), (2005, 2005), (2006, 2006), (2007, 2007), (2008, 2008), (2009, 2009), (2010, 2010), (2011, 2011), (2012, 2012), (2013, 2013), (2014, 2014), (2015, 2015), (2016, 2016)])),
                ('observation', models.CharField(default='', max_length=1000, verbose_name='observaci\xf3n', blank=True)),
            ],
            options={
                'verbose_name': 'convenci\xf3n',
                'verbose_name_plural': 'convenciones',
            },
        ),
        migrations.CreateModel(
            name='Denomination',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(unique=True, max_length=100, verbose_name='nombre', error_messages={'blank': 'El nombre no puede quedar vac\xedo'})),
            ],
            options={
                'verbose_name': 'denominaci\xf3n',
                'verbose_name_plural': 'denominaciones',
            },
        ),
        migrations.CreateModel(
            name='Language',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(unique=True, max_length=100, verbose_name='nombre', error_messages={'blank': 'El nombre no puede quedar vac\xedo'})),
            ],
            options={
                'verbose_name': 'idioma',
                'verbose_name_plural': 'idiomas',
            },
        ),
        migrations.CreateModel(
            name='Leader',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('first_names', models.CharField(default='', max_length=100, verbose_name='nombre(s)', error_messages={'blank': 'Nombre(s) no puede quedar vac\xedo'})),
                ('last_names', models.CharField(default='', max_length=100, verbose_name='apellido(s)', error_messages={'blank': 'Apellido(s) no puede quedar vac\xedo'})),
                ('birthdate', models.DateField(null=True, verbose_name='fecha de nacimiento', blank=True)),
                ('nationality', django_countries.fields.CountryField(blank=True, max_length=2, null=True, verbose_name='nacionalidad')),
                ('hometown', models.CharField(default='', max_length=100, verbose_name='lugar de nacimiento', blank=True)),
                ('current_residence', models.CharField(default='', max_length=100, verbose_name='residencia actual', blank=True)),
                ('phone_number', models.CharField(default='', max_length=50, verbose_name='tel\xe9fono', blank=True)),
                ('email', models.EmailField(default='', max_length=254, blank=True, error_messages={'invalid': 'El formato del email no es v\xe1lido'})),
                ('maximum_education_level', models.CharField(default='Ninguno', max_length=50, verbose_name='nivel m\xe1ximo de educaci\xf3n', choices=[('Primario', 'Primario'), ('Secundario', 'Secundario'), ('Universitario', 'Universitario'), ('Ninguno', 'Ninguno')])),
                ('theological_studies', models.CharField(default='', max_length=100, verbose_name='formaci\xf3n teol\xf3gica', blank=True)),
                ('profession', models.CharField(default='', max_length=100, verbose_name='profesi\xf3n', blank=True)),
                ('observation', models.CharField(default='', max_length=1000, verbose_name='observaci\xf3n', blank=True)),
                ('member_of', models.ForeignKey(verbose_name='miembro de', blank=True, to='swiep.Church', null=True)),
            ],
            options={
                'verbose_name': 'l\xedder',
                'verbose_name_plural': 'l\xedderes',
            },
        ),
        migrations.AddField(
            model_name='conference',
            name='denomination',
            field=models.ForeignKey(verbose_name='denominaci\xf3n', blank=True, to='swiep.Denomination', null=True),
        ),
        migrations.AddField(
            model_name='churchleader',
            name='leader',
            field=models.ForeignKey(verbose_name='l\xedder', to='swiep.Leader'),
        ),
        migrations.AddField(
            model_name='church',
            name='conference',
            field=models.ForeignKey(verbose_name='convenci\xf3n', blank=True, to='swiep.Conference', null=True),
        ),
        migrations.AddField(
            model_name='church',
            name='denomination',
            field=models.ForeignKey(verbose_name='denominaci\xf3n', blank=True, to='swiep.Denomination', null=True),
        ),
        migrations.AddField(
            model_name='church',
            name='language',
            field=models.ForeignKey(verbose_name='idioma', blank=True, to='swiep.Language', null=True),
        ),
        migrations.AddField(
            model_name='church',
            name='mother_church',
            field=models.ForeignKey(related_name='daughter_churches', verbose_name='iglesia madre', blank=True, to='swiep.Church', null=True),
        ),
        migrations.AlterUniqueTogether(
            name='churchleader',
            unique_together=set([('church', 'leader', 'ministry')]),
        ),
    ]
