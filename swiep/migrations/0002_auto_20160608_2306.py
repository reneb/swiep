# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('swiep', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='church',
            name='conference',
            field=models.ForeignKey(related_name='churches', on_delete=django.db.models.deletion.SET_NULL, verbose_name='convenci\xf3n', blank=True, to='swiep.Conference', null=True),
        ),
        migrations.AlterField(
            model_name='church',
            name='denomination',
            field=models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, verbose_name='denominaci\xf3n', blank=True, to='swiep.Denomination', null=True),
        ),
        migrations.AlterField(
            model_name='church',
            name='language',
            field=models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, verbose_name='idioma', blank=True, to='swiep.Language', null=True),
        ),
        migrations.AlterField(
            model_name='church',
            name='mother_church',
            field=models.ForeignKey(related_name='daughter_churches', on_delete=django.db.models.deletion.SET_NULL, verbose_name='iglesia madre', blank=True, to='swiep.Church', null=True),
        ),
        migrations.AlterField(
            model_name='churchleader',
            name='church',
            field=models.ForeignKey(related_name='leaders', verbose_name='iglesia', to='swiep.Church'),
        ),
        migrations.AlterField(
            model_name='churchleader',
            name='leader',
            field=models.ForeignKey(related_name='churches', verbose_name='l\xedder', to='swiep.Leader'),
        ),
        migrations.AlterField(
            model_name='conference',
            name='denomination',
            field=models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, verbose_name='denominaci\xf3n', blank=True, to='swiep.Denomination', null=True),
        ),
        migrations.AlterField(
            model_name='leader',
            name='member_of',
            field=models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, verbose_name='miembro de', blank=True, to='swiep.Church', null=True),
        ),
    ]
