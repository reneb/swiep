# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('swiep', '0003_auto_20160614_2114'),
    ]

    operations = [
        migrations.AlterField(
            model_name='church',
            name='department',
            field=models.CharField(default='', max_length=100, verbose_name='departamento', blank=True, choices=[('', ''), ('Alto Paraguay', 'Alto Paraguay'), ('Alto Paran\xe1', 'Alto Paran\xe1'), ('Amambay', 'Amambay'), ('Asunci\xf3n', 'Asunci\xf3n'), ('Boquer\xf3n', 'Boquer\xf3n'), ('Caaguaz\xfa', 'Caaguaz\xfa'), ('Caazap\xe1', 'Caazap\xe1'), ('Canindey\xfa', 'Canindey\xfa'), ('Central', 'Central'), ('Concepci\xf3n', 'Concepci\xf3n'), ('Cordillera', 'Cordillera'), ('Guair\xe1', 'Guair\xe1'), ('Itap\xfaa', 'Itap\xfaa'), ('Misiones', 'Misiones'), ('\xd1eembuc\xfa', '\xd1eembuc\xfa'), ('Paraguar\xed', 'Paraguar\xed'), ('Presidente Hayes', 'Presidente Hayes'), ('San Pedro', 'San Pedro')]),
        ),
    ]
