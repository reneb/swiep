# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('swiep', '0002_auto_20160608_2306'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='leader',
            options={'verbose_name': 'l\xedder', 'verbose_name_plural': 'l\xedderes', 'permissions': (('read_leader', 'Can read Leader'),)},
        ),
    ]
