var paramdict = '';
var current_search_field_state = '';
$(document).ready(function(){
    fill_paramdict();
    add_icon_to_sorted_col();
    set_searchbox_state();
    current_search_field_state = paramdict['search_field'];

    $('#description').click(function() {
        set_sort_field('short_description');
        goto_url();
        return false;
    });

    $('#priority').click(function() {
        set_sort_field('priority');
        goto_url();
        return false;
    });

    $('#technical-value').click(function() {
        set_sort_field('technical_value');
        goto_url();
        return false;
    });

    $('#business-value').click(function() {
        set_sort_field('business_value');
        goto_url();
        return false;
    });

    $('#est-delivery-date').click(function() {
        set_sort_field('estimated_delivery_date');
        goto_url();
        return false;
    });

    $('#est-work-duration').click(function() {
        set_sort_field('estimated_work_duration');
        goto_url();
        return false;
    });

    // Logic if clicking first search dropdown button
    $('.search-field-dropdown .dropdown-menu').find('a').click(function(e) {
        e.preventDefault();
        var search_field = $(this).attr("data-search-field");
        var isdigit = $(this).attr("data-isdigit");
        var concept = $(this).text();
        // Set the text of the dropdown-button
        $('.search-field-dropdown span#search_concept').text(concept);
        // Set the input values which later will be send as param in the url
        // Set which search field or column was selected
        $('.input-group #search_field').val(search_field);
        // Set if the selected field is of type digit
        $('.input-group #search_field_isdigit').val(isdigit);

        // The search_field short_descritpion has an set of corresponding search_types
        // and the other from search_field have another corresponding search_types
        // This if-else makes the corresponding search_types visible in the dropdown-list
        // if the user toggles between short_description and the other options from search_field
        // We have to handle the default search_type shown in the dropdown button too, but only if
        // the user changes between the two categorys (short_description, another fields)
        if (search_field == 'short_description' && current_search_field_state != 'short_description')
        {
            var link = $('a[data-search-type="ct"]');
            link.removeClass('hidden');
            link.click();
            $('a[data-search-type="gt"]').addClass('hidden');
            $('a[data-search-type="lt"]').addClass('hidden');
        }
        else if(search_field != 'short_description' &&
                (current_search_field_state == 'short_description' || !current_search_field_state))
        {
            $('a[data-search-type="eq"]').click();
            $('a[data-search-type="ct"]').addClass('hidden');
            $('a[data-search-type="gt"]').removeClass('hidden');
            $('a[data-search-type="lt"]').removeClass('hidden');
        }
        current_search_field_state = search_field;
    });

    // Logic if clicking second search dropdown button
    $('.search-type-dropdown .dropdown-menu').find('a').click(function(e) {
        e.preventDefault();
        var search_type = $(this).attr("data-search-type");
        var type = $(this).text();
        $('.search-type-dropdown span#search-type-concept').text(type);
        $('.input-group #search_type').val(search_type);
    });

    // Search Cancel Button
    // JS from http://www.bootply.com/130682
    var t = $(".hasclear");
    var clearer = $(".clearer");

    // Initially hide the cancel button
    clearer.toggle(Boolean(t.val()));

    // If the search input (hasclear) has text, show the button
    t.keyup(function () {
      t.next('span').toggle(Boolean(t.val()));
    });

    // A click clears the search input
    clearer.click(function () {
      $(this).prev('input').val('').focus();
      $(this).hide();
    });

});

function set_searchbox_state() {
    if(paramdict['search_field'])
    {
        // Set the text of the dropdown button (from search_field or column)
        // and the corresponding inputs
        var search_field_link = $('a[data-search-field='+ paramdict['search_field'] +']');
        if(search_field_link) {
            $('.search-field-dropdown span#search_concept').text(search_field_link.text());
            $('.input-group #search_field').val(paramdict['search_field']);
            var isdigit = search_field_link.attr('data-isdigit');
            if (isdigit)
                $('.input-group #search_field_isdigit').val(isdigit);
        }

        // If the user selected an search_field different than 'short_description'
        // change the selectable options. Per default the options corresponding to
        // short_description are visible (is equal, contains).
        if(paramdict['search_field'] != 'short_description'){
            $('a[data-search-type="ct"]').addClass('hidden');
            $('a[data-search-type="gt"]').removeClass('hidden');
            $('a[data-search-type="lt"]').removeClass('hidden');
        }
    }
    if(paramdict['search_type'])
    {
        // Set the text of the dropdown button (from search_type e.g equal, lower than, etc)
        // and the corresponding inputs
        var search_type_link = $('a[data-search-type='+ paramdict['search_type'] +']');
        if(search_type_link) {
            $('.search-type-dropdown span#search-type-concept').text(search_type_link.text());
            $('.input-group #search_type').val(paramdict['search_type']);
        }
    }
}

function set_sort_field(field) {
    if (paramdict['sort_field'] === field)
    // On the second click on a column change the order
        change_sort_order();
    else {
        paramdict['sort_field'] = field;
        // First click on a column sorts it ascending
        if (paramdict['sort_field_order'] == 'desc')
            paramdict['sort_field_order'] = 'asc';
    }
}

function change_sort_order() {
    if(paramdict['sort_field_order'] == 'desc')
        paramdict['sort_field_order'] = 'asc';
    else
        paramdict['sort_field_order'] = 'desc';
}

function add_icon_to_sorted_col() {
    var sort_field = paramdict['sort_field'];
    var order = paramdict['sort_field_order'];

    if(sort_field === 'short_description')
        if(order == 'desc')
            $('#description').find('.glyphicon').addClass('glyphicon-menu-up');
        else
            $('#description').find('.glyphicon').addClass('glyphicon-menu-down');

    else if(sort_field === 'priority')
        if(order == 'desc')
            $('#priority').find('.glyphicon').addClass('glyphicon-menu-up');
        else
            $('#priority').find('.glyphicon').addClass('glyphicon-menu-down');

    else if(sort_field === 'technical_value')
        if(order == 'desc')
            $('#technical-value').find('.glyphicon').addClass('glyphicon-menu-up');
        else
            $('#technical-value').find('.glyphicon').addClass('glyphicon-menu-down');

    else if(sort_field === 'business_value')
        if(order == 'desc')
            $('#business-value').find('.glyphicon').addClass('glyphicon-menu-up');
        else
            $('#business-value').find('.glyphicon').addClass('glyphicon-menu-down');

    else if(sort_field === 'estimated_delivery_date')
        if(order == 'desc')
            $('#est-delivery-date').find('.glyphicon').addClass('glyphicon-menu-up');
        else
            $('#est-delivery-date').find('.glyphicon').addClass('glyphicon-menu-down');

    else if(sort_field === 'estimated_work_duration')
        if(order == 'desc')
            $('#est-work-duration').find('.glyphicon').addClass('glyphicon-menu-up');
        else
            $('#est-work-duration').find('.glyphicon').addClass('glyphicon-menu-down');
}

function fill_paramdict(){
    // paramdict is an global variable or dictionary
    // containing all the GET params form the url.
    paramdict = {};
    var hash;
    var q = document.URL.split('?')[1];
    if(q != undefined){
        q = q.split('#')[0];
        q = q.split('&');
        for(var i = 0; i < q.length; i++){
            hash = q[i].split('=');
            paramdict[hash[0]] = hash[1];
        }
    }
}

function goto_url(){
    var url = '?';

    var temp = paramdict['q'];
    if(temp)
        url += 'q=' + temp + '&';

    temp = paramdict['search_field'];
    if(temp)
        url += 'search_field=' + temp + '&';

    temp = paramdict['search_type'];
    if(temp)
        url += 'search_type=' + temp + '&';

    temp = paramdict['search_field_isdigit'];
    if(temp)
        url += 'search_field_isdigit=' + temp + '&';

    temp = paramdict['sort_field'];
    if(temp)
        url += 'sort_field=' + temp + '&';

    temp = paramdict['sort_field_order'];
    if(temp)
        url += 'sort_field_order=' + temp;
    else
        // chop off the '&' at the end of url
        url = url.slice(0, -1);

    // After constructing the hole url,
    // we actually redirect to the url with the new GET params
    window.location.href = document.URL.split('?')[0].split('#')[0] + url;
}
