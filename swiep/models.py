# -*- coding: utf-8 -*-
#
# Copyright (C) 2017 Haiko Eitzen and Nico Epp
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

from __future__ import unicode_literals

from django.conf import settings
from django.core.exceptions import ValidationError, FieldError
from django.db import models
from django_countries.fields import CountryField
from djgeojson.fields import PointField
import datetime


class Church(models.Model):
    """
    This model represents the Church entity, the centerpiece of
    this system's data.

    A Church can be associated to Leaders and a Conference.
    """
    name = models.CharField("nombre", max_length=100, default='', error_messages={
        'blank': 'El nombre no puede quedar vacío',
    })
    denomination = models.ForeignKey(
        'Denomination', verbose_name="denominación",
        null=True, blank=True, on_delete=models.SET_NULL)
    conference = models.ForeignKey(
        'Conference', verbose_name="convención",
        null=True, blank=True, on_delete=models.SET_NULL,
        related_name='churches')
    language = models.ForeignKey(
        'Language', verbose_name="idioma",
        null=True, blank=True, on_delete=models.SET_NULL)
    phone_number = models.CharField("teléfono", max_length=50, blank=True, default='')
    email = models.EmailField(blank=True, default='', error_messages={
        'invalid': 'El formato del email no es válido',
    })
    web_site = models.CharField("sitio web", max_length=100, blank=True, default='')
    DPTO_CHOICES = [('Alto Paraguay', 'Alto Paraguay'),
                    ('Alto Paraná', 'Alto Paraná'),
                    ('Amambay', 'Amambay'),
                    ('Asunción', 'Asunción'),
                    ('Boquerón', 'Boquerón'),
                    ('Caaguazú', 'Caaguazú'),
                    ('Caazapá', 'Caazapá'),
                    ('Canindeyú', 'Canindeyú'),
                    ('Central', 'Central'),
                    ('Concepción', 'Concepción'),
                    ('Cordillera', 'Cordillera'),
                    ('Guairá', 'Guairá'),
                    ('Itapúa', 'Itapúa'),
                    ('Misiones', 'Misiones'),
                    ('Ñeembucú', 'Ñeembucú'),
                    ('Paraguarí', 'Paraguarí'),
                    ('Presidente Hayes', 'Presidente Hayes'),
                    ('San Pedro', 'San Pedro')]
    department = models.CharField("departamento", max_length=100, choices=DPTO_CHOICES, default='Asunción',
                                  error_messages={'blank': 'El departamento no puede quedar vacío',})
    city = models.CharField("ciudad", max_length=100, blank=True, default='')
    address = models.CharField("dirección", max_length=100, blank=True, default='')
    map_location = PointField("ubicación en el mapa", blank=True, default='')
    YEAR_CHOICES = []
    for r in range(1870, (datetime.datetime.now().year+1)):
        YEAR_CHOICES.append((r, r))
    foundation_year = models.IntegerField("año de fundación", choices=YEAR_CHOICES,
                                          default=datetime.datetime.now().year)
    membership = models.IntegerField("membresía", default=0)
    assistance = models.IntegerField("asistencia", default=0)
    owns_lot = models.BooleanField("posee terreno", default=False)
    owns_building = models.BooleanField("posee edificio", default=False)
    mother_church = models.ForeignKey(
        'self', verbose_name="iglesia madre",
        null=True, blank=True, on_delete=models.SET_NULL,
        related_name='daughter_churches')
    observation = models.CharField("observación", max_length=1000, blank=True, default='')
    # row_insert_time = models.DateTimeField(auto_now_add=True)
    # row_update_time = models.DateTimeField(auto_now=True)
    # row_insert_user = models.ForeignKey(User, related_name='created_clients')
    # row_update_user = models.ForeignKey(User, related_name='modified_clients')

    class Meta:
        verbose_name = 'iglesia'
        verbose_name_plural = 'iglesias'

    def __unicode__(self):
        return self.name


class Leader(models.Model):
    """
    A Leader is any person associated with a church ministry.

    An instance of Leader may be associated to zero, one or more Churches
    and a Church may associate zero, one or more Leaders.
    """
    first_names = models.CharField("nombre(s)", max_length=100, default='', error_messages={
        'blank': 'Nombre(s) no puede quedar vacío',
    })
    last_names = models.CharField("apellido(s)", max_length=100, default='', error_messages={
        'blank': 'Apellido(s) no puede quedar vacío',
    })
    member_of = models.ForeignKey(
        Church, verbose_name="miembro de",
        null=True, blank=True, on_delete=models.SET_NULL)
    birthdate = models.DateField("fecha de nacimiento", null=True, blank=True)
    nationality = CountryField("nacionalidad", null=True, blank=True)
    hometown = models.CharField("lugar de nacimiento", max_length=100, blank=True, default='')
    current_residence = models.CharField("residencia actual", max_length=100, blank=True, default='')
    phone_number = models.CharField("teléfono", max_length=50, blank=True, default='')
    email = models.EmailField(blank=True, default='', error_messages={
        'invalid': 'El formato del email no es válido',
    })
    EDUCATION_CHOICES = [('Primario', 'Primario'),
                         ('Secundario', 'Secundario'),
                         ('Universitario', 'Universitario'),
                         ('Ninguno', 'Ninguno')]
    maximum_education_level = models.CharField("nivel máximo de educación", max_length=50, choices=EDUCATION_CHOICES, default='Ninguno')
    theological_studies = models.CharField("formación teológica", max_length=100, blank=True, default='')
    profession = models.CharField("profesión", max_length=100, blank=True, default='')
    observation = models.CharField("observación", max_length=1000, blank=True, default='')

    class Meta:
        verbose_name = 'líder'
        verbose_name_plural = 'líderes'
        permissions = (("read_leader", "Can read Leader"),)

    @classmethod
    def all_as_choices(cls):
        yield (0, '---------')
        for leader in cls.objects.all():
            yield (leader.pk, '{} {}'.format(leader.first_names, leader.last_names))

    def __unicode__(self):
        return "{} {}".format(self.first_names, self.last_names)


class Conference(models.Model):
    """
    A Conference is a formally defined association of Churches.
    """
    name = models.CharField("nombre", max_length=100, default='', error_messages={
        'blank': 'El nombre no puede quedar vacío',
    })
    acronym = models.CharField("acrónimo", max_length=50, default='', error_messages={
        'blank': 'El acrónimo no puede quedar vacío',
    })
    denomination = models.ForeignKey(
        'Denomination', verbose_name="denominación",
        null=True, blank=True, on_delete=models.SET_NULL)
    phone_number = models.CharField("teléfono", max_length=50, blank=True, default='')
    email = models.EmailField(blank=True, default='', error_messages={
        'invalid': 'El formato del email no es válido',
    })
    web_site = models.CharField("sitio web", max_length=100, blank=True, default='')
    DPTO_CHOICES = [('', ''),
                    ('Alto Paraguay', 'Alto Paraguay'),
                    ('Alto Paraná', 'Alto Paraná'),
                    ('Amambay', 'Amambay'),
                    ('Asunción', 'Asunción'),
                    ('Boquerón', 'Boquerón'),
                    ('Caaguazú', 'Caaguazú'),
                    ('Caazapá', 'Caazapá'),
                    ('Canindeyú', 'Canindeyú'),
                    ('Central', 'Central'),
                    ('Concepción', 'Concepción'),
                    ('Cordillera', 'Cordillera'),
                    ('Guairá', 'Guairá'),
                    ('Itapúa', 'Itapúa'),
                    ('Misiones', 'Misiones'),
                    ('Ñeembucú', 'Ñeembucú'),
                    ('Paraguarí', 'Paraguarí'),
                    ('Presidente Hayes', 'Presidente Hayes'),
                    ('San Pedro', 'San Pedro')]
    department = models.CharField("departamento", max_length=100, choices=DPTO_CHOICES, blank=True, default='')
    city = models.CharField("ciudad", max_length=100, blank=True, default='')
    address = models.CharField("dirección", max_length=100, blank=True, default='')
    YEAR_CHOICES = []
    for r in range(1870, (datetime.datetime.now().year+1)):
        YEAR_CHOICES.append((r, r))
    foundation_year = models.IntegerField("año de fundación", choices=YEAR_CHOICES,
                                          default=datetime.datetime.now().year)
    observation = models.CharField("observación", max_length=1000, blank=True, default='')
    # row_insert_time = models.DateTimeField(auto_now_add=True)
    # row_update_time = models.DateTimeField(auto_now=True)
    # row_insert_user = models.ForeignKey(User, related_name='created_clients')
    # row_update_user = models.ForeignKey(User, related_name='modified_clients')

    class Meta:
        verbose_name = 'convención'
        verbose_name_plural = 'convenciones'

    def __unicode__(self):
        return self.name


class ChurchLeader(models.Model):
    """
    Association of a Church with a Leader
    """
    church = models.ForeignKey(
        Church, verbose_name="iglesia",
        on_delete=models.CASCADE,
        related_name='leaders')  # Refactor to churchleaders? 2 refs in views.py & church_form.html
    leader = models.ForeignKey(
        Leader, verbose_name="líder",
        on_delete=models.CASCADE, related_name='churches')
    ministry = models.CharField("ministerio", max_length=1000, default='Pastor')

    class Meta:
        unique_together = ('church', 'leader', 'ministry',)

    def clean(self):
        # Making sure the default ministry exists only once per church
        # Pseudocode: unique_together ('church', 'ministry') if ministry == default_ministry
        default_ministry = settings.DEFAULT_MINISTRY
        if self.ministry == default_ministry:
            if not self.church:
                raise FieldError('Field church has not been instanciated. '
                                 'Assign church to model before calling is_valid() on Form and '
                                 'before other calls which implicitly call Model.clean()')

            ministry_in_db = ChurchLeader.objects.filter(church=self.church,
                                                         ministry=default_ministry)
            if ministry_in_db.exists() and ministry_in_db.first().pk != self.pk:
                raise ValidationError({
                    'ministry': 'No puede haber más que un ministerio '
                                'con el rol {} en la misma iglesia'
                                .format(default_ministry)
                })

    def __unicode__(self):
        return "ChurchLeader(church.name: {}, leader.name: {} {}, ministry: {})".format(
            self.church.name, self.leader.first_names, self.leader.last_names, self.ministry)


class Denomination(models.Model):
    name = models.CharField("nombre", max_length=100, unique=True, error_messages={
        'blank': 'El nombre no puede quedar vacío',
    })

    class Meta:
        verbose_name = 'denominación'
        verbose_name_plural = 'denominaciones'

    def __unicode__(self):
        return self.name


class Language(models.Model):
    name = models.CharField("nombre", max_length=100, unique=True, error_messages={
        'blank': 'El nombre no puede quedar vacío',
    })

    class Meta:
        verbose_name = 'idioma'
        verbose_name_plural = 'idiomas'

    def __unicode__(self):
        return self.name
