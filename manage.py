#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2017 Haiko Eitzen and Nico Epp
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

from __future__ import unicode_literals
import os
import sys

if __name__ == "__main__":
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "IBA_swiep.settings")

    from django.core.management import execute_from_command_line

    execute_from_command_line(sys.argv)
