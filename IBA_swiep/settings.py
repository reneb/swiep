# -*- coding: utf-8 -*-
#
# Copyright (C) 2017 Haiko Eitzen and Nico Epp
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

from __future__ import unicode_literals
"""
Django settings for IBA_swiep project.
"""


# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(             # base/
    os.path.dirname(                    # base/IBA_swiep/
        os.path.abspath(__file__)))     # base/IBA_swiep/settings.py


ROOT_URLCONF = 'IBA_swiep.urls'
WSGI_APPLICATION = 'IBA_swiep.wsgi.application'
LOGIN_URL = '/login/'
LEAFLET_CONFIG = {
    'DEFAULT_CENTER': (-25.3, -57.5),
    'DEFAULT_ZOOM': 12,
    'MIN_ZOOM': 2,
    'MAX_ZOOM': 19,
}
COUNTRIES_FIRST = ['PY']
COUNTRIES_FIRST_REPEAT = True


INSTALLED_APPS = [
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django_forms_bootstrap',
    'django_countries',
    'djgeojson',
    'leaflet',
    'mailer',
    'swiep',
]
MIDDLEWARE_CLASSES = [
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.security.SecurityMiddleware',
]
TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]


# Sessions
# https://docs.djangoproject.com/en/1.8/topics/http/sessions/
SESSION_COOKIE_AGE = 24*60*60
SESSION_SAVE_EVERY_REQUEST = True
SESSION_EXPIRE_AT_BROWSER_CLOSE = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.8/howto/static-files/
STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR, 'staticfiles')
MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(BASE_DIR, 'mediafiles')


# messages
from django.contrib.messages import constants as messages
MESSAGE_TAGS = {
    # map django messages with ERROR level to bootstrap's alert-danger
    messages.ERROR: 'danger',
    # the other levels match perfectly
    # INFO -> alert-info
    # SUCCESS -> alert-success
    # WARNING -> alert-warning
}


# Internationalization
# https://docs.djangoproject.com/en/1.8/topics/i18n/
LANGUAGE_CODE = 'es-es'
TIME_ZONE = 'America/Asuncion'
USE_L10N = True
USE_TZ = True


# Security settings suggested when running 'manage.py check --deploy'
# https://docs.djangoproject.com/en/1.8/ref/middleware/
SECURE_HSTS_SECONDS = 3600      # set for more time ???
SECURE_HSTS_INCLUDE_SUBDOMAINS = True
SECURE_CONTENT_TYPE_NOSNIFF = True
SECURE_BROWSER_XSS_FILTER = True
SECURE_SSL_REDIRECT = True
SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')
SESSION_COOKIE_SECURE = True
CSRF_COOKIE_HTTPONLY = True
CSRF_COOKIE_SECURE = True
X_FRAME_OPTIONS = 'DENY'
ALLOWED_HOSTS = ['*']


# ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### #####
# ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### #####
# The following settings are different between development and production
# environments or contain sensitive data to be kept outside of the VCS
# ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### #####
# ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### #####


SECRET_KEY = os.environ.get('SECRET_KEY', 'd@vm=9+gjz73rrb*!l14an1*8s*5m!p6rk)#by=&u^jvbkda7l')


if 'PROD_ENV' not in os.environ:
    DEBUG = True
    # to use without HTTPS
    SECURE_HSTS_SECONDS = 0
    SECURE_SSL_REDIRECT = False
    SECURE_PROXY_SSL_HEADER = None
    SESSION_COOKIE_SECURE = False
    CSRF_COOKIE_SECURE = False


# Database
# https://docs.djangoproject.com/en/1.8/ref/settings/#databases
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}
if 'PROD_ENV' in os.environ:
    import dj_database_url
    DATABASES['default'] = dj_database_url.config()     # get data from os.environ
    DATABASES['default']['CONN_MAX_AGE'] = 500


# Email
# https://docs.djangoproject.com/en/1.8/topics/email/
# https://docs.djangoproject.com/en/1.8/ref/settings/#email
ADMINS = (
    ('Haiko Eitzen', 'haiko.eitzen@gmail.com'),
    ('Nico Epp', 'nicoeppfriesen@gmail.com'),
)
EMAIL_SUBJECT_PREFIX = '[SWIEP] '
EMAIL_BACKEND = 'mailer.backend.DbBackend'
MAILER_EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'


# Application settings (Hard coded)

# Ministry with an special threatment.
#   Shows up as a dropdown in CRUD of church.
#
#   Before you change the default ministry name in production, make sure
#   there no ministry with this name in Db. Then convert existing default
#   ministry names to the new name
DEFAULT_MINISTRY = 'Pastor'
