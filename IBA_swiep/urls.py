# -*- coding: utf-8 -*-
#
# Copyright (C) 2017 Haiko Eitzen and Nico Epp
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

from __future__ import unicode_literals
"""IBA_swiep URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import patterns, include, url
from django.contrib import admin
from swiep import views


urlpatterns = patterns('',
    url(r'^$',
        views.map_view,
        name='map'
        ),
    url(r'^login/$',
        views.login_view,
        name='login'
        ),
    url(r'^logout/$',
        views.logout_view,
        name='logout'
        ),
    url(r'^home/$',
        views.home,
        name='home'
        ),
    url(r'^management/$',
        views.management,
        name='management'
        ),
    url(r'^churches/$',
        views.church_list,
        name='church_list'
        ),
    url(r'^churches/create/$',
        views.church_create,
        name='church_create'
        ),
    url(r'^churches/(?P<church_id>\d+)/$',
        views.church_read,
        name='church_read'
        ),
    url(r'^churches/(?P<church_id>\d+)/update/$',
        views.church_update,
        name='church_update'
        ),
    url(r'^churches/(?P<church_id>\d+)/delete/$',
        views.church_delete,
        name='church_delete'
        ),
    url(r'^churches/(?P<church_id>\d+)/ministry/create/$',
        views.churchleader_create_at_church,
        name='churchleader_create_at_church'
        ),
    url(r'^churches/(?P<church_id>\d+)/ministry/(?P<churchleader_id>\d+)/$',
        views.churchleader_read_at_church,
        name='churchleader_read_at_church'
        ),
    url(r'^churches/(?P<church_id>\d+)/ministry/(?P<churchleader_id>\d+)/update/$',
        views.churchleader_update_at_church,
        name='churchleader_update_at_church'
        ),
    url(r'^churches/(?P<church_id>\d+)/ministry/(?P<churchleader_id>\d+)/delete/$',
        views.churchleader_delete_at_church,
        name='churchleader_delete_at_church'
        ),
    url(r'^leaders/$',
        views.leader_list,
        name='leader_list'
        ),
    url(r'^leaders/create/$',
        views.leader_create,
        name='leader_create'
        ),
    url(r'^leaders/(?P<leader_id>\d+)/$',
        views.leader_read,
        name='leader_read'
        ),
    url(r'^leaders/(?P<leader_id>\d+)/update/$',
        views.leader_update,
        name='leader_update'
        ),
    url(r'^leaders/(?P<leader_id>\d+)/delete/$',
        views.leader_delete,
        name='leader_delete'
        ),
    url(r'^leaders/(?P<leader_id>\d+)/ministry/create/$',
        views.churchleader_create_at_leader,
        name='churchleader_create_at_leader'
        ),
    url(r'^leaders/(?P<leader_id>\d+)/ministry/(?P<churchleader_id>\d+)/$',
        views.churchleader_read_at_leader,
        name='churchleader_read_at_leader'
        ),
    url(r'^leaders/(?P<leader_id>\d+)/ministry/(?P<churchleader_id>\d+)/update/$',
        views.churchleader_update_at_leader,
        name='churchleader_update_at_leader'
        ),
    url(r'^leaders/(?P<leader_id>\d+)/ministry/(?P<churchleader_id>\d+)/delete/$',
        views.churchleader_delete_at_leader,
        name='churchleader_delete_at_leader'
        ),
    url(r'^conferences/$',
        views.conference_list,
        name='conference_list'
        ),
    url(r'^conferences/create/$',
        views.conference_create,
        name='conference_create'
        ),
    url(r'^conferences/(?P<conference_id>\d+)/$',
        views.conference_read,
        name='conference_read'
        ),
    url(r'^conferences/(?P<conference_id>\d+)/update/$',
        views.conference_update,
        name='conference_update'
        ),
    url(r'^conferences/(?P<conference_id>\d+)/delete/$',
        views.conference_delete,
        name='conference_delete'
        ),
   url(r'^users/$',
       views.user_list,
       name='user_list'
       ),
   url(r'^users/create/$',
       views.user_create,
       name='user_create'
       ),
   url(r'^users/(?P<user_id>\d+)/$',
       views.user_read,
       name='user_read'
       ),
   url(r'^users/(?P<user_id>\d+)/update/$',
       views.user_update,
       name='user_update'
       ),
   url(r'^users/(?P<user_id>\d+)/update/password/$',
       views.change_password,
       name='change_password'
       ),
   url(r'^users/(?P<user_id>\d+)/delete/$',
       views.user_delete,
       name='user_delete'
       ),
   url(r'^groups/$',
       views.group_list,
       name='group_list'
       ),
   url(r'^groups/create/$',
       views.group_create,
       name='group_create'
       ),
   url(r'^groups/(?P<group_id>\d+)/$',
       views.group_read,
       name='group_read'
       ),
   url(r'^groups/(?P<group_id>\d+)/update/$',
       views.group_update,
       name='group_update'
       ),
   url(r'^groups/(?P<group_id>\d+)/delete/$',
       views.group_delete,
       name='group_delete'
       ),
   url(r'^others/$',
       views.other_list,
       name='other_list'
       ),
   url(r'^others/denominations/create/$',
       views.denomination_create,
       name='denomination_create'
       ),
   url(r'^others/denominations/(?P<denomination_id>\d+)/delete/$',
       views.denomination_delete,
       name='denomination_delete'
       ),
   url(r'^others/languages/create/$',
       views.language_create,
       name='language_create'
       ),
   url(r'^others/languages/(?P<language_id>\d+)/delete/$',
       views.language_delete,
       name='language_delete'
       ),
)
