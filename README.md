**SWIEP**

*Sistema Web de Información Eclesial del Paraguay*

Developed by **Haiko Eitzen** and **Nico Epp** for **Marturia** Research Group of the **Instituto
 Bíblico Asunción**

This system intends to serve as a database of churches, conferences (confederations of churches) and
leaders for the evangelical churches of Paraguay. It includes or will include the following
features:    
- Map of churches in Paraguay    
- Lists of churches, conferences and leaders     
- Profiles on churches, conferences and leaders  
  - Basic read-only info for non-authenticated users     
  - More complete information for authenticated users    
  - Edit profiles or add new ones (only super users)     
- User authentication and authorization, several levels of permissions   
- Basic statistic reports    